author: olivier larrieu

email: larrieuolivierad@gmail.com

place: toulouse (france)

# AutoModelSerializer
    allow to serialize the relations of a model.
    but is unusefull because rest_framework depth parameter does the same.

## sample

```python
# models
from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return "category {} {}".format(self.name, self.id)


class Product(models.Model):
    name = models.CharField(max_length=50)
    category = models.ForeignKey(Category, on_delete=models.PROTECT, related_name="products")

    def __str__(self):
        return "product {} {}".format(self.name, self.id)


class OwnerType(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return "type {} {}".format(self.name, self.id)


class ProductOwner(models.Model):
    name = models.CharField(max_length=50)
    product = models.ForeignKey(Product, on_delete=models.PROTECT, related_name="owners")
    type = models.ForeignKey(OwnerType, on_delete=models.PROTECT, related_name="owners")

    def __str__(self):
        return "{} {}".format(self.name, self.id)

# view
from rest_framework.viewsets import ModelViewSet
from drf_auto_serializer.serializers import AutoModelSerializer
from product.models import Product


class ProductSerializer(AutoModelSerializer):

    class Meta:
        model = Product
        fields = ['id', 'name', 'category', 'owners', ]
        # specify if you want to serialize or not relations
        serialize_relations = True


class ProductView(ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

# with depth parameter
class ProductSerializer(ModelSerializer):

    class Meta:
        model = Product
        fields = ['id', 'name', 'category', 'owners', ]
        depth = 3


class ProductView(ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

```

## will render

```json
[
    {
        "id": 1,
        "name": "product 0",
        "category": {
            "id": 1,
            "name": "category 0"
        },
        "owners": [
            {
                "id": 1,
                "name": "owner 1",
                "product": {
                    "id": 1,
                    "name": "product 0",
                    "category": {
                        "id": 1,
                        "name": "category 0"
                    }
                },
                "type": {
                    "id": 1,
                    "name": "type 0"
                }
            }
        ]
    }
]
```

## TODO
    should allow create and update both with relation pk or relation data.
 