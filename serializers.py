from rest_framework import serializers


class AutoModelSerializer(serializers.ModelSerializer):
    """
    Serialize the model relations in one shot.
    """
    @classmethod
    def get_serializer_for_model(cls, model_class):

        class Serializer(AutoModelSerializer):

            class Meta:
                model = model_class
                fields = [ field.name for field in model_class._meta.fields]
                serialize_relations = True

        return Serializer

    def to_representation(self, instance):
        data = super().to_representation(instance)
        try:
            if self.Meta.serialize_relations:
                relations_direct = [field for field in self.Meta.model._meta.fields if field.is_relation]
                relations_many = [field for field in self.Meta.model._meta.related_objects]
                relations = [*relations_direct, *relations_many]

                if relations:
                    for field_rel in relations:
                        extra_kwargs = {}
                        if field_rel.one_to_many:
                            rel_instance = field_rel.related_model.objects.filter(pk__in=data[field_rel.name])
                            extra_kwargs["many"] = True
                        else:
                            rel_instance = field_rel.related_model.objects.get(pk=data[field_rel.name])
                            
                        rel_serializer = AutoModelSerializer.get_serializer_for_model(field_rel.related_model)(rel_instance, **extra_kwargs)
                        data[field_rel.name] = rel_serializer.data
        except Exception as err:
            pass
        return data
